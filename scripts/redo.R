library(igraph)
library(parallel)
devtools::load_all(".")


redo=readRDS("redo.RDS")

outfolder="out_ngroup_Normalizedfix50"

dir.create(outfolder)
cl=makeCluster(45,type="FORK",outfile=file.path(outfolder,"log.txt"))
explorQ=parLapply(cl,rownames(redo),function(i)
                  {
                      print(paste(i,"/",nrow(redo)))
                      fname=file.path(outfolder,paste0("subex_",i,"_",paste0(redo[i,],collapse="_"),".RDS"))
                      if(!file.exists(fname)){
                      a=Sys.time()
                      resi=replicate(100,
                                     {
                                         pop.info<-list()
                                         n.groups<-round(redo[i,1])
                                         p.og<-redo[i,2]
                                         pop.info[[1]]<-pop.gen(n.groups,s.groups,n.I=1)[[1]]
                                         net<-mat.net(pop.info=pop.info,p.ig=p.ig,p.og=p.og,s.group=s.groups,dist.eff=dist.eff,plot=F)
                                         results<-runsimu(tstep=900,network=net,popinf=pop.info,S_I=S_I,I_R=I_R,R_S=R_S)
                                         results$summaries[900,]
                                     })
                      print(Sys.time()-a)
                      saveRDS(file=file.path(outfolder,paste0("subex_",i,"_",paste0(redo[i,],collapse="_"),".RDS")),resi)
                      }
                      else{
                          resi=readRDS(fname)
                      }

                      resi
                  })

saveRDS(file=file.path(outfolder,"semi_final.RDS"),explorQ)

stopCluster(cl)

