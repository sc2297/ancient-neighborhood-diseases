[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10822248.svg)](https://doi.org/10.5281/zenodo.10822248)


# Ancient cities & spread of pathogens

This repository contains the data and code for our paper:

> Modelling cultural responses to disease spread in Neolithic Trypillia mega-settlements



This directory contains:

-   [:file_folder: R/](./R/): R source code with model and function to run  the analysis
-   [:file_folder: data/](./data/): Data generate by the scripts in `./scripts/` and used in the analysis.
-   [:file_folder: figures/](./figures/): Scripts to generate the figures in the paper
-   [:file_folder: scripts/](./scripts/): Scripts to explore the model and data

The code in [📄R/basefunction.R](./R/basefunction.R) is based on Matthew Silk code developed for this paper https://link.springer.com/article/10.1007/s00265-021-03055-8 . The original code has been wrapped withing easier-to-call function and all algorithm within these functions have been vectorized to speed up run time and allow parallelisation and exhaustive exploration of the parameter space of the model. 

The two scripts used to generated the data used in the paper are:

- [📄scripts/extendedExploration.R](./scripts/extendedExploration.R): script to run the series of simulation on hypothetical networs
- [📄scripts/virusonRealData.R](./scripts/virusonRealData.R): script that uses a summary of the real data to run the virus on the three phases

They both require parallelisation to be run in reasonable time. The script [📄scripts/minimalexplo.R](./scripts/minimalexplo.R), can be used to simulate a simple, unique simulation with given set of parameters.
